--get germplasm name by joining germplsm table with the names table
--then join with atributs table to get aval

--explain
with nval as ( 
select gms.germplsm.gid as gid,gpid1,gpid2,gdate,nval from gms.germplsm
inner join gms.names on gms.germplsm.gid = gms.names.gid
)
select nval.gid,gpid1,gpid2,gdate,nval,aval from nval
inner join gms.atributs on gms.atributs.gid = nval.gid